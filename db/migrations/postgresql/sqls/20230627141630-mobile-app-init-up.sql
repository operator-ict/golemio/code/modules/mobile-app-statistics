CREATE TABLE mobileappstatistics_appstore (
	begin_day date NOT NULL,
	app_id varchar(50) NOT NULL,
	app_name varchar(50) NOT NULL,
	"version" varchar(50) NOT NULL,
	event_type varchar(50) NOT NULL,
	device varchar(50) NOT NULL,
	country varchar(50) NOT NULL,
	event_count int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamp NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamp NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT mobileappstatistics_appstore_pk PRIMARY KEY (begin_day, app_id, app_name, version, event_type, device, country)
);

CREATE INDEX idx_mobileappstatistics_appstore_begin_day ON mobileappstatistics_appstore USING btree (begin_day);
CREATE INDEX mobileappstatistics_appstore_create_batch ON mobileappstatistics_appstore USING btree (create_batch_id);
CREATE INDEX mobileappstatistics_appstore_update_batch ON mobileappstatistics_appstore USING btree (update_batch_id);


CREATE TABLE mobileappstatistics_playstore (
	begin_day date NOT NULL,
	package_name varchar(256) NOT NULL,
	daily_device_installs int4 NULL,
	daily_user_installs int4 NULL,
	install_events int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamp NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamp NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT mobileappstatistics_playstore_pk PRIMARY KEY (begin_day, package_name)
);

CREATE INDEX idx_mobileappstatistics_playstore_begin_day ON mobileappstatistics_playstore USING btree (begin_day);
CREATE INDEX mobileappstatistics_playstore_create_batch ON mobileappstatistics_playstore USING btree (create_batch_id);
CREATE INDEX mobileappstatistics_playstore_update_batch ON mobileappstatistics_playstore USING btree (update_batch_id);
