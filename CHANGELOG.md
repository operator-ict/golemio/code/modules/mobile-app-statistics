# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.1.10] - 2024-11-28

### Added

-   AsyncAPI documentation ([integration-engine#264](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/264))

## [1.1.9] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.1.8] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.1.7] - 2024-04-08

### Changed

-   migrate axios to undici ([core#99](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/99))

## [1.1.6] - 2023-07-31

### Changed

-   use DateTime wrapper instead of moment.js ([#7](https://gitlab.com/operator-ict/golemio/code/modules/mobile-app-statistics/-/issues/7))

## [1.1.5] - 2023-07-17

-   schema separation `public` -> `mobile_app_statistics` ([#8](https://gitlab.com/operator-ict/golemio/code/modules/mobile-app-statistics/-/issues/8))

## [1.1.4] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [1.1.3] - 2023-03-22

### Fixed

-   Fix play store csv headers name inconsistency ([#6](https://gitlab.com/operator-ict/golemio/code/modules/mobile-app-statistics/-/issues/6))

## [1.1.2] - 2023-03-08

### Changed

-   Convert Mongoose schema definitions to JSON schemas ([general#435](https://gitlab.com/operator-ict/golemio/code/general/-/issues/435))

## [1.1.1] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.1.0] - 2023-01-23

### Changed

-   Migrate to npm

## [1.0.8] - 2023-01-04

### Security

-   Update `jsonwebtoken` to `^9.0.0`

## [1.0.7] - 2022-11-29

### Changed

-   Update gitlab-ci template

## [1.0.6] - 2022-06-21

### Changed

-   Typescript version update from 4.4.4 to 4.6.4

