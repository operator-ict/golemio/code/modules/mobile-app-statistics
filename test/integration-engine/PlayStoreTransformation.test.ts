import { PlayStoreTransformation } from "#ie/PlayStoreTransformation";
import { PlayStoreModel } from "#sch/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";

chai.use(chaiAsPromised);

describe("PlayStoreTransformation", () => {
    let transformation: PlayStoreTransformation;
    let testSourceData: any[];
    let validator: JSONSchemaValidator;

    before(() => {
        validator = new JSONSchemaValidator("PlayStoreModelValidator", PlayStoreModel.jsonSchema);
    });

    beforeEach(async () => {
        transformation = new PlayStoreTransformation();
        const buffer = fs.readFileSync(__dirname + "/data/mobileappstatistics_playstore-datasource.json");
        testSourceData = JSON.parse(Buffer.from(buffer).toString("utf8"));
    });

    it("should has name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("PlayStoreTransformation");
    });

    it("should has transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform collection", async () => {
        const data = await transformation.transform(testSourceData);
        await expect(validator.Validate(data)).to.be.fulfilled;

        for (let i = 0, imax = data.length; i < imax; i++) {
            expect(data[i]).to.have.property("begin_day");
            expect(data[i]).to.have.property("daily_device_installs");
            expect(data[i]).to.have.property("daily_device_installs");
            expect(data[i]).to.have.property("install_events");
            expect(data[i]).to.have.property("package_name");
        }
    });
});
