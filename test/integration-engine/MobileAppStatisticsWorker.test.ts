import jwt from "jsonwebtoken";
import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { config } from "@golemio/core/dist/integration-engine/config";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import { MobileAppStatisticsWorker } from "#ie/MobileAppStatisticsWorker";

describe("MobileAppStatisticsWorker", () => {
    let worker: MobileAppStatisticsWorker;
    let sandbox: SinonSandbox;
    let testDataAppStore: any[];
    let testDataPlayStore: any[];
    let testTransformedData: number[];

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sandbox.stub(RedisConnector, "getConnection");

        testDataAppStore = [
            { "Begin Date": "03/09/2020", SKU: "test1" },
            { "Begin Date": "03/09/2020", SKU: "test2" },
        ];
        testDataPlayStore = [
            { Date: "2020-09-03", "Package Name": "test1" },
            { Date: "2020-09-03", "Package Name": "test2" },
        ];
        testTransformedData = [1, 2];

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() => Object.assign({ define: sandbox.stub() }));

        sandbox.stub(jwt, "sign").callsFake(() => "jwtstring");

        sandbox.stub(config, "datasources").value({
            AppStoreConnectCredentials: {},
            AppStoreConnect: "",
        });

        worker = new MobileAppStatisticsWorker();

        sandbox.stub(worker["appStoreDataSource"], "getAll").callsFake(() => Promise.resolve(testDataAppStore));
        sandbox.stub(worker["appStoreTransformation"], "transform").callsFake(() => Promise.resolve(testTransformedData));
        sandbox.stub(worker["appStoreModel"], "save");
        sandbox.stub(worker["playStoreDataSource"], "getAll").callsFake(() => Promise.resolve(testDataPlayStore));
        sandbox.stub(worker["playStoreTransformation"], "transform").callsFake(() => Promise.resolve(testTransformedData as any));
        sandbox.stub(worker["playStoreModel"], "save");
        sandbox.stub(worker["redisModel"], "hget").callsFake(() => Promise.resolve({ data: 1, filepath: 11 }.data));
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should calls the correct methods by refreshAppStoreDataInDB method", async () => {
        await worker.refreshAppStoreDataInDB({});
        sandbox.assert.calledOnce(worker["appStoreDataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["appStoreTransformation"].transform as SinonSpy);
        sandbox.assert.calledWith(worker["appStoreTransformation"].transform as SinonSpy, testDataAppStore);
        sandbox.assert.calledOnce(worker["appStoreModel"].save as SinonSpy);
        sandbox.assert.callOrder(
            worker["appStoreDataSource"].getAll as SinonSpy,
            worker["appStoreTransformation"].transform as SinonSpy,
            worker["appStoreModel"].save as SinonSpy
        );
    });

    it("should calls the correct methods by refreshPlayStoreDataInDB method", async () => {
        await worker.refreshPlayStoreDataInDB({});
        sandbox.assert.calledOnce(worker["playStoreDataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["playStoreTransformation"].transform as SinonSpy);
        sandbox.assert.calledWith(worker["playStoreTransformation"].transform as SinonSpy, testDataPlayStore);
        sandbox.assert.calledOnce(worker["playStoreModel"].save as SinonSpy);
        sandbox.assert.callOrder(
            worker["playStoreDataSource"].getAll as SinonSpy,
            worker["playStoreTransformation"].transform as SinonSpy,
            worker["playStoreModel"].save as SinonSpy
        );
    });
});
