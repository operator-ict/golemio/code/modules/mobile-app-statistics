import { AppStoreTransformation } from "#ie/AppStoreTransformation";
import { AppStoreModel } from "#sch/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";

chai.use(chaiAsPromised);

describe("AppStoreTransformation", () => {
    let transformation: AppStoreTransformation;
    let testSourceData: any[];
    let validator: JSONSchemaValidator;

    before(() => {
        validator = new JSONSchemaValidator("AppStoreModelValidator", AppStoreModel.jsonSchema);
    });

    beforeEach(async () => {
        transformation = new AppStoreTransformation();
        const buffer = fs.readFileSync(__dirname + "/data/mobileappstatistics_appstore-datasource.json");
        testSourceData = JSON.parse(Buffer.from(buffer).toString("utf8"));
    });

    it("should has name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("AppStoreTransformation");
    });

    it("should has transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform collection", async () => {
        const data = await transformation.transform(testSourceData);
        await expect(validator.Validate(data)).to.be.fulfilled;

        for (let i = 0, imax = data.length; i < imax; i++) {
            expect(data[i]).to.have.property("app_id");
            expect(data[i]).to.have.property("app_name");
            expect(data[i]).to.have.property("begin_day");
            expect(data[i]).to.have.property("country");
            expect(data[i]).to.have.property("device");
            expect(data[i]).to.have.property("event_count");
            expect(data[i]).to.have.property("event_type");
            expect(data[i]).to.have.property("version");
        }
    });
});
