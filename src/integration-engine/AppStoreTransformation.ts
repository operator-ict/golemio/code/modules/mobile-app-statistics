import { IAppStoreInputDto } from "#sch/datasources/interfaces/IAppStoreInputDto";
import { IAppStoreOutputDto } from "#sch/models/interfaces/IAppStoreOutputDto";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { DateTime } from "@golemio/core/dist/helpers";

export class AppStoreTransformation extends BaseTransformation implements ITransformation {
    public name = "AppStoreTransformation";

    protected transformElement = async (element: IAppStoreInputDto): Promise<IAppStoreOutputDto> => {
        return {
            app_id: element.SKU,
            app_name: element.Title,
            begin_day: DateTime.fromFormat(element["Begin Date"], "LL/dd/yyyy").setTimeZone("Europe/Prague").format("yyyy-LL-dd"),
            country: element["Country Code"],
            device: element.Device,
            event_count: parseInt(element.Units),
            event_type: element["Product Type Identifier"],
            version: element.Version,
        };
    };
}
