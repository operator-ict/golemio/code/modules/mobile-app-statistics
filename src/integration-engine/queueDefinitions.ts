import { config } from "@golemio/core/dist/integration-engine/config";
import { IQueueDefinition } from "@golemio/core/dist/integration-engine/queueprocessors";
import { MobileAppStatistics } from "#sch";
import { MobileAppStatisticsWorker } from "#ie/MobileAppStatisticsWorker";

const queueDefinitions: IQueueDefinition[] = [
    {
        name: MobileAppStatistics.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + "." + MobileAppStatistics.name.toLowerCase(),
        queues: [
            {
                name: "refreshAppStoreDataInDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: MobileAppStatisticsWorker,
                workerMethod: "refreshAppStoreDataInDB",
            },
            {
                name: "refreshPlayStoreDataInDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: MobileAppStatisticsWorker,
                workerMethod: "refreshPlayStoreDataInDB",
            },
        ],
    },
];

export { queueDefinitions };
