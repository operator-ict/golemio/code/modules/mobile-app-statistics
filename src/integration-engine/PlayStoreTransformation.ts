import { IPlayStoreInputDto, IPlayStoreInputFileDto } from "#sch/datasources/interfaces/IPlayStoreInputDto";
import { IPlayStoreOutputDto } from "#sch/models/interfaces/IPlayStoreOutputDto";
import { CSVDataTypeStrategy } from "@golemio/core/dist/integration-engine/datasources";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { DateTime } from "@golemio/core/dist/helpers";

export class PlayStoreTransformation extends BaseTransformation implements ITransformation {
    public name = "PlayStoreTransformation";

    /**
     * Overrides BaseTransformation::transform
     */
    public transform = async (data: IPlayStoreInputFileDto[]): Promise<IPlayStoreOutputDto[]> => {
        const toTransform = data.map(async (file) => {
            return this.transformFile(file);
        });

        const transformed = await Promise.all(toTransform);
        const promises = transformed.flat().map((element) => {
            return this.transformElement(element);
        });

        const results = await Promise.all(promises);
        return results.filter((r) => r);
    };

    protected transformElement = async (element: IPlayStoreInputDto): Promise<IPlayStoreOutputDto> => {
        return {
            begin_day: DateTime.fromFormat(element.date, "yyyy-LL-dd", { timeZone: "Europe/Prague" }).format("yyyy-LL-dd"),
            daily_device_installs: element.daily_device_installs ? parseInt(element.daily_device_installs) : null,
            daily_user_installs: element.daily_user_installs ? parseInt(element.daily_user_installs) : null,
            install_events: element.install_events ? parseInt(element.install_events) : null,
            package_name: element.package_name,
        };
    };

    private transformFile = async (file: IPlayStoreInputFileDto): Promise<IPlayStoreInputDto[]> => {
        const csvDataTypeStrategy = new CSVDataTypeStrategy({
            fastcsvParams: {
                // normalize headers to prevent lower/upper case inconsistency
                headers: (headers: string[]) => headers.map((h) => h?.toLowerCase().replace(" ", "_")),
            },
            subscribe: (json) => json,
        });
        return csvDataTypeStrategy.parseData(file.data);
    };
}
