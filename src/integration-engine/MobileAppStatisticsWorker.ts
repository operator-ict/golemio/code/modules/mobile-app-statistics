import { MobileAppStatistics } from "#sch";
import { appStoreDtoSchema, playStoreDtoSchema } from "#sch/datasources";
import { IPlayStoreInputFileDto } from "#sch/datasources/interfaces/IPlayStoreInputDto";
import { AppStoreModel, PlayStoreModel } from "#sch/models";
import { DateTime, dateTime } from "@golemio/core/dist/helpers";
import { config } from "@golemio/core/dist/integration-engine/config";
import {
    CSVDataTypeStrategy,
    DataSource,
    GoogleCloudStorageProtocolStrategy,
    JSONDataTypeStrategy,
} from "@golemio/core/dist/integration-engine/datasources";
import { HTTPRequestProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPRequestProtocolStrategy";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { PostgresModel, RedisModel } from "@golemio/core/dist/integration-engine/models";
import { BaseWorker } from "@golemio/core/dist/integration-engine/workers";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { sign } from "jsonwebtoken";
import { AppStoreTransformation, PlayStoreTransformation } from "./";

export class MobileAppStatisticsWorker extends BaseWorker {
    private appStoreDataSource: DataSource;
    private appStoreTransformation: AppStoreTransformation;
    private appStoreModel: PostgresModel;
    private playStoreDataSource: DataSource;
    private playStoreTransformation: PlayStoreTransformation;
    private playStoreModel: PostgresModel;
    private redisModel: RedisModel;

    constructor() {
        super();
        this.appStoreDataSource = new DataSource(
            "AppStoreDataSource",
            null as any,
            new CSVDataTypeStrategy({
                fastcsvParams: { headers: true, delimiter: "\t" },
                subscribe: (json: any) => json,
            }),
            new JSONSchemaValidator("AppStoreDataSourceValidator", appStoreDtoSchema)
        );
        this.appStoreTransformation = new AppStoreTransformation();
        this.appStoreModel = new PostgresModel(
            "AppStoreModel",
            {
                pgSchema: MobileAppStatistics.pgSchema,
                outputSequelizeAttributes: AppStoreModel.attributeModel,
                pgTableName: AppStoreModel.tableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("AppStoreModelValidator", AppStoreModel.jsonSchema)
        );

        this.playStoreDataSource = new DataSource(
            "PlayStoreDataSource",
            new GoogleCloudStorageProtocolStrategy({
                bucketName: "pubsite_prod_rev_01447282685199189351",
                filesFilter: (f: File) => f.name.indexOf("_overview.csv") !== -1,
                filesPrefix: "stats/installs",
                keyFilename: config.datasources.PlayStoreKeyFilename,
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator("PlayStoreDataSourceValidator", playStoreDtoSchema)
        );
        this.playStoreTransformation = new PlayStoreTransformation();
        this.playStoreModel = new PostgresModel(
            "PlayStoreModel",
            {
                pgSchema: MobileAppStatistics.pgSchema,
                outputSequelizeAttributes: PlayStoreModel.attributeModel,
                pgTableName: PlayStoreModel.tableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("PlayStoreModelValidator", PlayStoreModel.jsonSchema)
        );

        this.redisModel = new RedisModel(
            MobileAppStatistics.name + "Model",
            {
                isKeyConstructedFromData: false,
                prefix: "files",
            },
            null
        );
    }

    public refreshAppStoreDataInDB = async (msg: any): Promise<void> => {
        let date: DateTime;
        try {
            // setting custom date from message data
            const input = JSON.parse(msg.content.toString());
            if (input.date) {
                date = dateTime(new Date(input.date), { timeZone: "Europe/Prague" });
                log.debug(`Custom date: ${date} was used.`);
            } else {
                throw new Error("Input message must contain 'date' property.");
            }
        } catch (err) {
            // setting default date (normal situation)
            date = dateTime(new Date());
            date.subtract(2, "days");
        }

        log.debug(`Used date: ${date.format("yyyy-LL-dd")}`);

        const bearerToken = sign(
            {
                aud: "appstoreconnect-v1",
                exp: Math.floor(Date.now() / 1000) + 20 * 60,
                iss: config.datasources.AppStoreConnectCredentials.iss,
            },
            config.datasources.AppStoreConnectCredentials.private_key,
            {
                header: {
                    alg: "ES256",
                    kid: config.datasources.AppStoreConnectCredentials.kid,
                    typ: "JWT",
                },
            }
        );

        this.appStoreDataSource.setProtocolStrategy(
            new HTTPRequestProtocolStrategy({
                headers: {
                    Accept: "application/a-gzip",
                    Authorization: `Bearer ${bearerToken}`,
                },
                compression: "gzip",
                responseType: "arrayBuffer",
                method: "GET",
                url: config.datasources.AppStoreConnect.replace(":reportDate", date.format("yyyy-LL-dd")),
            })
        );

        const data = await this.appStoreDataSource.getAll();
        const transformedData = await this.appStoreTransformation.transform(data);
        await this.appStoreModel.save(transformedData);
    };

    public refreshPlayStoreDataInDB = async (msg: any): Promise<void> => {
        const data: IPlayStoreInputFileDto[] = await this.playStoreDataSource.getAll();
        const inputData = await Promise.all(
            data.map(async (file) => {
                file.data = await this.redisModel.hget(file.filepath);
                return file;
            })
        );
        const transformedData = await this.playStoreTransformation.transform(inputData);
        await this.playStoreModel.save(transformedData);
    };
}
