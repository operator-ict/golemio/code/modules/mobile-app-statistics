import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes, ModelIndexesOptions } from "@golemio/core/dist/shared/sequelize";
import { IAppStoreOutputDto } from "./interfaces/IAppStoreOutputDto";

export class AppStoreModel extends Model<IAppStoreOutputDto> implements IAppStoreOutputDto {
    public static tableName = "mobileappstatistics_appstore";

    declare app_id: string;
    declare app_name: string;
    declare begin_day: string;
    declare country: string;
    declare device: string;
    declare event_count: number;
    declare event_type: string;
    declare version: string;

    public static attributeModel: ModelAttributes<AppStoreModel> = {
        app_id: {
            primaryKey: true,
            type: DataTypes.STRING,
        },
        app_name: {
            primaryKey: true,
            type: DataTypes.STRING,
        },
        begin_day: {
            primaryKey: true,
            type: DataTypes.DATE,
        },
        country: {
            primaryKey: true,
            type: DataTypes.STRING,
        },
        device: {
            primaryKey: true,
            type: DataTypes.STRING,
        },
        event_count: DataTypes.INTEGER,
        event_type: {
            primaryKey: true,
            type: DataTypes.STRING,
        },
        version: {
            primaryKey: true,
            type: DataTypes.STRING,
        },
    };

    public static indexesOptions: ModelIndexesOptions[] = [
        {
            unique: false,
            using: "BTREE",
            name: "idx_mobileappstatistics_appstore_begin_day",
            fields: ["begin_day"],
        },
        {
            unique: false,
            using: "BTREE",
            name: "mobileappstatistics_appstore_create_batch",
            fields: ["create_batch_id"],
        },
        {
            unique: false,
            using: "BTREE",
            name: "mobileappstatistics_appstore_update_batch",
            fields: ["update_batch_id"],
        },
    ];

    public static jsonSchema: JSONSchemaType<IAppStoreOutputDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                app_id: { type: "string" },
                app_name: { type: "string" },
                begin_day: { type: "string" },
                country: { type: "string" },
                device: { type: "string" },
                event_count: { type: "number" },
                event_type: { type: "string" },
                version: { type: "string" },
            },
            required: ["app_id", "app_name", "begin_day", "country", "device", "event_count", "event_type", "version"],
        },
    };
}
