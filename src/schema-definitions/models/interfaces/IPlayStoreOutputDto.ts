export interface IPlayStoreOutputDto {
    begin_day: string;
    daily_device_installs: number | null;
    daily_user_installs: number | null;
    install_events: number | null;
    package_name: string;
}
