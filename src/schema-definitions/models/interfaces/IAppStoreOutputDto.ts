export interface IAppStoreOutputDto {
    app_id: string;
    app_name: string;
    begin_day: string;
    country: string;
    device: string;
    event_count: number;
    event_type: string;
    version: string;
}
