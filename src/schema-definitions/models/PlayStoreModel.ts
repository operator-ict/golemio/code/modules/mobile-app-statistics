import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes, ModelIndexesOptions } from "@golemio/core/dist/shared/sequelize";
import { IPlayStoreOutputDto } from "./interfaces/IPlayStoreOutputDto";

export class PlayStoreModel extends Model<IPlayStoreOutputDto> implements IPlayStoreOutputDto {
    public static tableName = "mobileappstatistics_playstore";

    declare begin_day: string;
    declare daily_device_installs: number | null;
    declare daily_user_installs: number | null;
    declare install_events: number | null;
    declare package_name: string;

    public static attributeModel: ModelAttributes<PlayStoreModel> = {
        begin_day: {
            primaryKey: true,
            type: DataTypes.DATE,
        },
        daily_device_installs: DataTypes.INTEGER,
        daily_user_installs: DataTypes.INTEGER,
        install_events: DataTypes.INTEGER,
        package_name: {
            primaryKey: true,
            type: DataTypes.STRING,
        },
    };

    public static indexesOptions: ModelIndexesOptions[] = [
        {
            unique: false,
            using: "BTREE",
            name: "idx_mobileappstatistics_playstore_begin_day",
            fields: ["begin_day"],
        },
        {
            unique: false,
            using: "BTREE",
            name: "mobileappstatistics_playstore_create_batch",
            fields: ["create_batch_id"],
        },
        {
            unique: false,
            using: "BTREE",
            name: "mobileappstatistics_playstore_update_batch",
            fields: ["update_batch_id"],
        },
    ];

    public static jsonSchema: JSONSchemaType<IPlayStoreOutputDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                begin_day: {
                    type: "string",
                    format: "date",
                },
                daily_device_installs: {
                    oneOf: [
                        {
                            type: "integer",
                        },
                        {
                            type: "null",
                            nullable: true,
                        },
                    ],
                },
                daily_user_installs: {
                    oneOf: [
                        {
                            type: "integer",
                        },
                        {
                            type: "null",
                            nullable: true,
                        },
                    ],
                },
                install_events: {
                    oneOf: [
                        {
                            type: "integer",
                        },
                        {
                            type: "null",
                            nullable: true,
                        },
                    ],
                },
                package_name: {
                    type: "string",
                },
            },
            required: ["begin_day", "package_name"],
        },
    };
}
