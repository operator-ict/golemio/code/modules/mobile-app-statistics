import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IPlayStoreInputFileDto } from "./interfaces/IPlayStoreInputDto";

export const playStoreDtoSchema: JSONSchemaType<IPlayStoreInputFileDto[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            filepath: { type: "string" },
            name: { type: "string" },
            data: { type: "string", nullable: true },
        },
        required: ["filepath", "name"],
    },
};
