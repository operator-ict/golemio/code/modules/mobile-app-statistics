export interface IPlayStoreInputFileDto {
    filepath: string;
    name: string;
    data?: string;
}

export interface IPlayStoreInputDto {
    date: string;
    daily_device_installs: string;
    daily_user_installs: string;
    install_events: string;
    package_name: string;
}
