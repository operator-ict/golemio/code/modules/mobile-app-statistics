export interface IAppStoreInputDto {
    "Apple Identifier": string;
    "Begin Date": string;
    CMB: string;
    Category: string;
    Client: string;
    "Country Code": string;
    "Currency of Proceeds": string;
    "Customer Currency": string;
    "Customer Price": string;
    Developer: string;
    "Developer Proceeds": string;
    Device: string;
    "End Date": string;
    "Order Type": string;
    "Parent Identifier": string;
    Period: string;
    "Preserved Pricing": string;
    "Proceeds Reason": string;
    "Product Type Identifier": string;
    "Promo Code": string;
    Provider: string;
    "Provider Country": string;
    SKU: string;
    Subscription: string;
    "Supported Platforms": string;
    Title: string;
    Units: string;
    Version: string;
}
